module dodosuko

go 1.18

require github.com/faiface/beep v1.1.0

require (
	github.com/hajimehoshi/go-mp3 v0.3.1 // indirect
	github.com/hajimehoshi/oto v0.7.1 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	golang.org/x/exp v0.0.0-20190731235908-ec7cb31e5a56 // indirect
	golang.org/x/image v0.0.0-20200801110659-972c09e46d76 // indirect
	golang.org/x/mobile v0.0.0-20210208171126-f462b3930c8f // indirect
	golang.org/x/sys v0.0.0-20200918174421-af09f7315aff // indirect
)
