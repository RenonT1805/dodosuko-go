package main

import (
	"fmt"
	"log"
	"math/rand"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

var answer = []string{
	"ドド",
	"スコ",
	"スコ",
	"スコ",
	"ドド",
	"スコ",
	"スコ",
	"スコ",
	"ドド",
	"スコ",
	"スコ",
	"スコ",
}

func check(ddsk *[]string) bool {
	if len(*ddsk) != len(answer) {
		return false
	}
	for i := range answer {
		if (*ddsk)[i] != answer[i] {
			*ddsk = []string{}
			return false
		}
	}
	return true
}

func main() {
	dodoFile, err := os.Open("dodo.mp3")
	if err != nil {
		log.Fatal(err)
	}
	dodoStream, format, err := mp3.Decode(dodoFile)
	if err != nil {
		log.Fatal(err)
	}
	defer dodoStream.Close()

	sukoFile, err := os.Open("suko.mp3")
	if err != nil {
		log.Fatal(err)
	}
	sukoStream, format, err := mp3.Decode(sukoFile)
	if err != nil {
		log.Fatal(err)
	}
	defer sukoStream.Close()

	speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/10))

	rand.Seed(time.Now().UnixNano())
	vocabulary := []string{"ドド", "スコ"}

	var ddsk []string
	for {
		tmp := rand.Intn(2)
		ddsk = append(ddsk, vocabulary[tmp])

		done := make(chan bool)
		if tmp == 0 {
			dodoStream.Seek(0)
			speaker.Play(beep.Seq(dodoStream, beep.Callback(func() {
				done <- true
			})))
		} else {
			sukoStream.Seek(0)
			speaker.Play(beep.Seq(sukoStream, beep.Callback(func() {
				done <- true
			})))
		}
		<-done

		fmt.Print(vocabulary[tmp])
		if check(&ddsk) {
			fmt.Println("ラブ注入♡")
			break
		}
	}
}
